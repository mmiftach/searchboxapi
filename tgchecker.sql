/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50721
Source Host           : localhost:3306
Source Database       : tgchecker

Target Server Type    : MYSQL
Target Server Version : 50721
File Encoding         : 65001

Date: 2018-11-14 13:54:28
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for api_users
-- ----------------------------
DROP TABLE IF EXISTS `api_users`;
CREATE TABLE `api_users` (
  `email` varchar(255) NOT NULL,
  `api_key` varchar(255) NOT NULL,
  `hit` int(11) NOT NULL,
  PRIMARY KEY (`email`),
  UNIQUE KEY `api_key` (`api_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of api_users
-- ----------------------------
INSERT INTO `api_users` VALUES ('dian@petanikode.com', '123', '68');

-- ----------------------------
-- Table structure for maintable
-- ----------------------------
DROP TABLE IF EXISTS `maintable`;
CREATE TABLE `maintable` (
  `id` int(11) NOT NULL,
  `CompanyName` varchar(255) DEFAULT NULL,
  `DueDate` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `InvoiceSource` varchar(255) DEFAULT NULL,
  `Value` int(255) DEFAULT NULL,
  `Currency` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of maintable
-- ----------------------------
INSERT INTO `maintable` VALUES ('1', 'PT.Asal.com', '2018-11-10 00:00:00', 'aaaa', '1', '1');
INSERT INTO `maintable` VALUES ('2', 'PT.bukan.com', '2018-11-05 16:15:39', 'vvvv', '2', '2');
INSERT INTO `maintable` VALUES ('3', 'PT.salah.com', '2018-11-04 15:51:15', 'dddd', '333', '444');
INSERT INTO `maintable` VALUES ('4', 'PT.Asal.com', '2018-11-10 00:00:00', 'aaaa', '1', '1');
INSERT INTO `maintable` VALUES ('5', 'PT.Asal.com', '2018-11-14 11:03:13', null, null, null);
INSERT INTO `maintable` VALUES ('6', 'PT.Asal.com', '2018-11-14 11:03:15', null, null, null);
INSERT INTO `maintable` VALUES ('7', 'PT.Asal.com', '2018-11-14 11:03:23', null, null, null);
INSERT INTO `maintable` VALUES ('8', 'PT.Asal.com', '2018-11-14 11:03:24', null, null, null);
INSERT INTO `maintable` VALUES ('9', 'PT.Asal.com', '2018-11-14 11:03:25', null, null, null);
INSERT INTO `maintable` VALUES ('10', 'PT.Asal.com', '2018-11-14 11:03:27', null, null, null);
INSERT INTO `maintable` VALUES ('11', 'PT.Asal.com', '2018-11-14 11:03:30', null, null, null);
SET FOREIGN_KEY_CHECKS=1;
